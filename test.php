<?php

spl_autoload_register(function ($class_name) {
    $class_name = str_replace('\\', '/', $class_name);

    //echo "src/" . $class_name . '.php' . "\n";
    include "src/" . $class_name . '.php';
});


$asn = new teamCymru\asn;
var_dump($asn->asn(36352)->get()->parse());

$origin = new teamCymru\origin;
var_dump($origin->ip('8.8.8.8')->get()->parse());

$origin6 = new teamCymru\origin6;
var_dump($origin6->ip('2001:4860:4860::8888')->get()->parse());

$peer = new teamCymru\peer;
var_dump($peer->ipv6('2001:4860:4860::8888')->get()->parse());

$peer = new teamCymru\peer;
var_dump($peer->ipv4('8.8.8.8')->get()->parse());