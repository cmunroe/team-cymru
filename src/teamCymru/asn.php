<?php

namespace teamCymru;

class asn extends core
{
    /**
     * DNS Path.
     */
    protected $path = "asn.cymru.com";

    /**
     * Data Headers.
     */
    protected $headers = array('AS', 'CC', 'Registry', 'Allocated', 'Name');

    public function asn(int $asn){

        $asn = "AS" . $asn;

        $this->query = $asn . "." . $this->path;

        return $this;

    }
    
}