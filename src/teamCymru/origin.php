<?php

namespace teamCymru;

/**
 * Lookup data via DNS about an IPv4 address.
 * 
 *  $data = $cymru->ip('192.168.1.1)->get()->parse();
 */
class origin extends core
{ 

    /**
     * DNS Path.
     */
    protected $path = "origin.asn.cymru.com";

    /**
     * Data Headers.
     */
    protected $headers = array('AS', 'Prefix', 'CC', 'Registry', 'Allocated');

    /**
     * Specify the IP we wish to use.
     *
     * @param string $ip
     * @return this
     */
    public function ip(string $ip){

        $this->ipv4($ip);

        return $this;

    }



    
}