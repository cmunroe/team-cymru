<?php

namespace teamCymru;

class core
{

    /**
     * What we will be looking up.
     */
    protected $query = false;

    /**
     * Which DNS Server we would like to use.
     */
    protected $dnsServer = "1.1.1.1";

    /**
     * Our returned data from our get command.
     */
    protected $data = false;

    /**
     * Get our DNS Record.
     *
     * @return this
     */
    public function get(){

        if($this->query === false){

            return false; 

        }

        $this->data = dns_get_record($this->query, DNS_TXT);

        return $this;

    }


    /**
     * Parse the data into an Array.
     *
     * @return array data.
     */
    public function parse(){

        if(!isset($this->data[0]['txt'])){

            return false;

        }

        // Explode the Data.
        $data = explode('|', $this->data[0]['txt']);

        $response = array();

        foreach($data as $key => $item){

            $response[$this->headers[$key]] = trim($item);

        }

        return $response;

    }

    /**
     * Reverse the IP Address. 
     *
     * @param string $ip
     * @return string IP Address
     */
    protected function flipv4(string $ip){

        $ip = explode(".", $ip);

        $ip = array_reverse($ip);

        $ipReversed = null;

        foreach($ip as $octet){

            $ipReversed .= $octet . ".";

        }

        return $ipReversed;

    }

    /**
     * Specify our IP Address we want to lookup.
     *
     * @param string $ip
     * @return void
     */
    public function ipv4(string $ip){

        $ipReversed = $this->flipv4($ip);

        $this->query = $ipReversed . $this->path;

        return $this;

    }

    /**
     * Reverse the IP Address. 
     *
     * @param string $ip
     * @return string IP Address
     */
    protected function flipv6(string $ip){

        $ip = \str_replace(':', '', $ip);

        $ip = str_split($ip);

        $ip = array_reverse($ip);

        $ipReversed = null;

        foreach($ip as $digit){

            $ipReversed .= $digit . ".";

        }

        return $ipReversed;

    }

    /**
     * Specify our IP Address we want to lookup.
     *
     * @param string $ip
     * @return void
     */
    public function ipv6(string $ip){

        $ipReversed = $this->flipv6($ip);

        $this->query = $ipReversed . $this->path;

        return $this;

    }
    
}