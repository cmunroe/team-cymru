<?php

namespace teamCymru;

class peer extends core
{

    /**
     * DNS Path.
     */
    protected $path = "peer.asn.cymru.com";

    /**
     * Data Headers.
     */
    protected $headers = array('Peers', 'Prefix', 'CC', 'Registry', 'Allocated');


    public function parse(){

        $response = core::parse();

        if(isset($response['Peers'])){

            $response['Peers'] = explode(' ', $response['Peers']);

        }

        return $response;
        
    }   
    
}