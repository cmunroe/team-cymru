<?php

namespace teamCymru;


class origin6 extends core
{
    /**
     * DNS Path.
     */
    protected $path = "origin6.asn.cymru.com";

    /**
     * Data Headers.
     */
    protected $headers = array('AS', 'Prefix', 'CC', 'Registry', 'Allocated');

    /**
     * Specify the IP we wish to use.
     *
     * @param string $ip
     * @return this
     */
    public function ip(string $ip){

        $this->ipv6($ip);

        return $this;

    }

}